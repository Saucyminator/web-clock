---
---

'use strict';

var update = function () {
  $('#datetime').html(moment().format("HH:mm:ss"));
};

$(document).ready(function () {
  update();
  setInterval(update, 1000);
});
